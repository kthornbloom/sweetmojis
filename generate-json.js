const fs = require('fs');
const path = require('path');

const imagesDirectory = 'public/images';
const jsonFilePath = 'generated-json/images.json';

const imageFileNames = fs.readdirSync(imagesDirectory);

const jsonData = {
  images: imageFileNames.map((fileName) => path.join(imagesDirectory, fileName))
};

// Create the necessary directories if they don't exist
const directoryPath = path.dirname(jsonFilePath);
if (!fs.existsSync(directoryPath)) {
  fs.mkdirSync(directoryPath, { recursive: true });
}

fs.writeFileSync(jsonFilePath, JSON.stringify(jsonData, null, 2));

console.log(`JSON file generated at ${jsonFilePath}`);

