Get 'yer 'mojis:  
https://kthornbloom.gitlab.io/sweetmojis/

## Contributing:

1) Pull the repo
2) Add 32px image(s) to public/images
3) Run generate-json.js
4) Do a merge request or just push to master if you have rights.
